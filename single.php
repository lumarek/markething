<?php get_header(); ?>

    <section class="l-article">
    <div class="c-container c-container--small">

    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    <?php get_template_part( 'entry' ); ?>
    <?php if ( ! post_password_required() ) comments_template( '', true ); ?>
    <?php endwhile; endif; ?>

    </div>
    </section>

    <?php get_template_part( 'nav', 'below-single' ); ?>

<?php get_sidebar(); ?>
<?php get_footer(); ?>