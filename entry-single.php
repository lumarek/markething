<section class="l-article">
    <div class="c-container c-container--small">
        <h2 class="l-article__heading c-heading c-heading--primary c-heading--large"><?php the_title(); ?></h2>
        <div class="l-article__wrapper">
            <div class="l-article__aside">
                <div class="c-aside">
                    <div class="c-aside__img"><?php echo get_avatar( get_the_author_meta('email'), '240' ); ?>
                    <div class="c-stamp c-stamp--single c-stamp--top-left">
                        <p class="c-stamp__text">Autor článku</p>
                    </div>
                    </div>
                    <div class="c-aside__author">			
                    <h3 class="c-aside__heading"><?php the_author_posts_link(); ?></h3>
                    <p><?php the_author_meta("description"); ?></p>
                    </div>
                    <!-- <div class="c-aside__author-social">
                    <h3 class="c-aside__heading">Sleduj autora</h3>
                    <ul>
                        <li><a href="/" target="_blank">Facebook</a></li>
                        <li><a href="/" target="_blank">Twitter</a></li>
                        <li><a href="/" target="_blank">Instagram</a></li>
                    </ul>
                    </div> -->
                </div>
            </div>
            <div class="l-article__content">
                <div style="font-style: italic;"><?php the_excerpt(); ?></div>              
                <?php get_template_part( 'entry', 'content' ); ?>
            </div>
        </div>
    </div>
</section>