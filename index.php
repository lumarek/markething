<?php get_header(); ?>
<div class="l-intro">
    <div class="c-container c-container--small">


    <!-- First post -->
    <?php if (!is_paged() && !is_single() && have_posts()): the_post();
        get_template_part( 'entry', 'first' );
    endif; ?>

    <?php if (is_home() && !is_single()) : ?> <div class="l-intro__more"> <div class="c-row c-row--large"> <?php endif; ?>

    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

        <?php get_template_part( 'entry' ); ?>
        <?php //comments_template(); ?>

    <?php endwhile; endif; ?>

    <?php if (is_home() && !is_single()) : ?> </div></div> <?php endif; ?>

    <?php get_template_part( 'nav', 'below' ); ?>
    
    </div>
</div>

<?php get_sidebar(); ?>
<?php get_footer(); ?>