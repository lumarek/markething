<?php
add_action( 'after_setup_theme', 'markething_setup' );
function markething_setup() {
    add_theme_support( 'title-tag' );
    add_theme_support( 'automatic-feed-links' );
    add_theme_support( 'post-thumbnails' );
    global $content_width;
    if ( ! isset( $content_width ) ) $content_width = 640;
    register_nav_menus(
        array( 'main-menu' => __( 'Main Menu', 'markething' ) )
    );
}

add_action( 'wp_enqueue_scripts', 'markething_load_scripts' );
function markething_load_scripts() {
    wp_enqueue_script( 'jquery' );
    wp_enqueue_script( 'markething', get_template_directory_uri() . '/js/index.js', array(), '1.0.0', true );
}

add_action( 'comment_form_before', 'markething_enqueue_comment_reply_script' );
function markething_enqueue_comment_reply_script() {
    if ( get_option( 'thread_comments' ) ) { wp_enqueue_script( 'comment-reply' ); }
}

add_filter( 'the_title', 'markething_title' );
function markething_title( $title ) {
    if ( $title == '' ) {
        return '&rarr;';
    } else {
        return $title;
    }
}

add_filter( 'wp_title', 'markething_filter_wp_title' );
function markething_filter_wp_title( $title ) {
    return $title . esc_attr( get_bloginfo( 'name' ) );
}

add_action( 'widgets_init', 'markething_widgets_init' );
function markething_widgets_init() {
    // register_sidebar( array (
    //     'name' => __( 'Sidebar Widget Area', 'markething' ),
    //     'id' => 'primary-widget-area',
    //     'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
    //     'after_widget' => "</li>",
    //     'before_title' => '<h3 class="widget-title">',
    //     'after_title' => '</h3>',
    // ) );
}

function markething_custom_pings( $comment ) {
    $GLOBALS['comment'] = $comment;
    ?>
    <li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>"><?php echo comment_author_link(); ?></li>
    <?php 
}

add_filter( 'get_comments_number', 'markething_comments_number' );
function markething_comments_number( $count ) {
    if ( !is_admin() ) {
        global $id;
        $comments_by_type = &separate_comments( get_comments( 'status=approve&post_id=' . $id ) );
        return count( $comments_by_type['comment'] );
    } else {
        return $count;
    }
}


function wpdocs_excerpt_more( $more ) {
    return '...';
}
add_filter( 'excerpt_more', 'wpdocs_excerpt_more' );

function markething_customize_register( $wp_customize ) {
    $wp_customize->add_setting( 'facebook' , array('default'   => '') );
    $wp_customize->add_setting( 'twitter' , array('default'   => '') );
    $wp_customize->add_setting( 'instagram' , array('default'   => '') );
    $wp_customize->add_setting( 'youtube' , array('default'   => '') );
    $wp_customize->add_setting( 'rss' , array('default'   => '') );
    $wp_customize->add_setting( 'about_us' , array('default'   => '') );
    $wp_customize->add_setting( 'about_us-page' , array('default'   => '') );

    $wp_customize->add_section( 'social_links' , array(
        'title'      => __( 'Sociální sítě', 'markething' ),
        'priority'   => 30,
    ) );

    $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'facebook', array(
        'label'      => __( 'Facebook', 'markething' ),
        'type'       => 'url',
        'section'    => 'social_links',
        'settings'   => 'facebook',
    ) ) );

    $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'twitter', array(
        'label'      => __( 'Twitter', 'markething' ),
        'type'       => 'url',
        'section'    => 'social_links',
        'settings'   => 'twitter',
    ) ) );


    $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'instagram', array(
        'label'      => __( 'Instagram', 'markething' ),
        'type'       => 'url',
        'section'    => 'social_links',
        'settings'   => 'instagram',
    ) ) );


    $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'youtube', array(
        'label'      => __( 'Youtube', 'markething' ),
        'type'       => 'url',
        'section'    => 'social_links',
        'settings'   => 'youtube',
    ) ) );


    $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'rss', array(
        'label'      => __( 'RSS', 'markething' ),
        'type'       => 'url',
        'section'    => 'social_links',
        'settings'   => 'rss',
    ) ) );

    $wp_customize->add_section( 'about_us' , array(
        'title'      => __( 'O nás', 'markething' ),
        'priority'   => 30,
    ) );

    $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'about_us', array(
        'label'      => __( 'Popis do patičky', 'markething' ),
        'type'       => 'textarea',
        'section'    => 'about_us',
        'settings'   => 'about_us',
    ) ) );

    $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'about_us-page', array(
        'label'      => __( 'Více o nás', 'markething' ),
        'type'       => 'dropdown-pages',
        'section'    => 'about_us',
        'settings'   => 'about_us-page',
    ) ) );
    
}
add_action( 'customize_register', 'markething_customize_register' );
 
// remove width & height attributes from images
//
function remove_img_attr ($html)
{
    return preg_replace('/(width|height)="\d+"\s/', "", $html);
}
 
add_filter( 'post_thumbnail_html', 'remove_img_attr' );

add_filter( 'img_caption_shortcode', 'cleaner_caption', 10, 3 );

function cleaner_caption( $output, $attr, $content ) {

	/* We're not worried abut captions in feeds, so just return the output here. */
	if ( is_feed() )
		return $output;

	/* Set up the default arguments. */
	$defaults = array(
		'id' => '',
		'align' => 'aligncenter',
		'width' => '910',
		'caption' => ''
	);

	/* Merge the defaults with user input. */
	$attr = shortcode_atts( $defaults, $attr );

	/* If the width is less than 1 or there is no caption, return the content wrapped between the &#091;caption&#093;&lt; tags. */
	if ( 1 > $attr['width'] || empty( $attr['caption'] ) )
		return $content;

	/* Set up the attributes for the caption div. */
	$attributes = ( !empty( $attr['id'] ) ? ' id="' . esc_attr( $attr['id'] ) . '"' : '' );
	$attributes .= ' class="l-article__caption wp-caption ' . esc_attr( $attr['align'] ) . '"';
	$attributes .= ' style="width: ' . esc_attr( $attr['width'] ) . 'px"';

	/* Open the caption &lt;div>. */
	$output = '<div' . $attributes .'>';

	/* Allow shortcodes for the content the caption was created for. */
	$output .= do_shortcode( $content );

	/* Append the caption text. */
	$output .= '<p class="l-article__caption-text wp-caption-text">' . $attr['caption'] . '</p>';

	/* Close the caption &lt;/div>. */
	$output .= '</div>';

	/* Return the formatted, clean caption. */
	return $output;
}
