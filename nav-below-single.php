<section class="l-more">
    <div class="c-container c-container--small">
    <h2 class="l-more__heading c-heading c-heading--secondary c-heading--small">Přečtěte si také</h2>
    <div class="l-more__content">
        <div class="c-row c-row--small">

        <?php
            $category = get_the_category();
        ?>

    <?php
	//for use in the loop, list 5 post titles related to first tag on current post
        $tags = wp_get_post_tags($post->ID);

        if ($tags) {
            $first_tag = $tags[0]->term_id;
            $args=array(
                'tag__in' => array($first_tag),
                'post__not_in' => array($post->ID),
                'posts_per_page'=>2,
                'caller_get_posts'=>1
            );
            $my_query = new WP_Query($args);
            if( $my_query->have_posts() ) {

			    while ($my_query->have_posts()) : $my_query->the_post(); ?>

            <div class="c-col c-col--2"><a class="c-card" href="<?php the_permalink() ?>">
                <div class="c-card__img"><?php { the_post_thumbnail(array(515, 285)); } ?></div>
                <div class="c-card__info">
                    <p class="c-card__heading"><?php the_title(); ?></p>
            </div></a></div>

    <?php endwhile; } wp_reset_query(); } ?>

        </div>
    </div>

    <?php if ( $category[0] ) : ?>
            <div class="l-more__action"><a class="c-btn c-btn--large c-btn--secondary" href="<?php echo get_category_link( $category[0]->term_id ) ?>">Více z kategorie <?php echo $category[0]->cat_name ?></a></div>        
    <?php endif; ?>
    
    </div>
</section>