<?php if (is_paged()): ?>

<div class="l-intro__action">
    <span class="next-posts"><?php next_posts_link('&laquo; Starší články') ?></span>
    <span class="prev-posts"><?php previous_posts_link('Novější články &raquo;') ?></span>
</div>


<?php else: ?>

    <?php if(get_next_posts_link()): ?>
        <div class="l-intro__action"><a class="c-btn c-btn--large c-btn--primary" href="<?php echo get_next_posts_page_link(); ?>">Další články</a></div>
    <?php endif; ?>

<?php endif; ?>