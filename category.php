<?php get_header(); ?>

<section class="l-article">
    <div class="c-container c-container--small">
        
        <section class="l-article">
            <div class="c-container c-container--small">
                <h2 class="l-article__heading c-heading c-heading--primary c-heading--large"><?php single_cat_title(); ?></h2>
                <div class="l-article__wrapper">
                <div class="l-article__content">
                    <?php if ( '' != category_description() ) echo apply_filters( 'archive_meta', '<div class="archive-meta">' . category_description() . '</div>' ); ?>
                </div>
                    
                </div>
            </div>
        </section>

        <?php if (!is_paged() && !is_single() && have_posts()): the_post();
            get_template_part( 'entry', 'first' );
        endif; ?>


        <div class="l-intro__more" style="padding-top: 40px"> <div class="c-row c-row--large">

        <?php rewind_posts(); ?>

        <?php while ( have_posts() ) : the_post(); ?>
            <?php get_template_part( 'entry' ); ?>
        <?php endwhile; ?>

        </div></div>

        <?php get_template_part( 'nav', 'below' );  ?>

    </div>
</section>

<?php get_sidebar(); ?>
<?php get_footer(); ?>