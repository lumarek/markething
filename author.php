<?php get_header(); ?>

<section class="l-article">
    <div class="c-container c-container--small">
        

        <section class="l-article">
            <div class="c-container c-container--small">
                <h2 class="l-article__heading c-heading c-heading--primary c-heading--large"><?php the_author_link(); ?></h2>
                <div class="l-article__wrapper">
                    <div class="l-article__aside"><div class="c-aside">
                        <?php echo get_avatar( get_the_author_meta('email'), '240' ); ?>
                    </div></div>

                <div class="l-article__content">
                    <?php if ( '' != get_the_author_meta( 'user_description' ) ) echo apply_filters( 'archive_meta', '<div class="archive-meta">' . get_the_author_meta( 'user_description' ) . '</div>' ); ?>
                </div>
                    
                </div>
            </div>
        </section>

        <div class="l-" style="padding-top: 40px"> <div class="c-row c-row--large">

        <?php rewind_posts(); ?>

        <?php while ( have_posts() ) : the_post(); ?>
            <?php get_template_part( 'entry' ); ?>
        <?php endwhile; ?>

        </div></div>

    </div>
</section>

<?php get_sidebar(); ?>
<?php get_footer(); ?>