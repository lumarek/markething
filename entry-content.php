<?php $category = get_the_category(); ?>


<div class="l-article__img">
    <?php { the_post_thumbnail(array(910, 0)); } ?>
    <div class="c-stamp c-stamp--top-left">
    <p class="c-stamp__date"><?php the_time( get_option( 'date_format' ) ); ?></p>
    <div class="c-stamp__link"><a href="<?php echo get_category_link( $category[0]->term_id ) ?>"><?php echo $category[0]->cat_name ?></a></div>
    </div>
</div> 

<?php the_content(null, true); ?>
