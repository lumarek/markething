<?php get_header(); ?>

<section class="l-article">
    <div class="c-container c-container--small">
        
        <section class="l-article">
            <div class="c-container c-container--small">
                <h2 class="l-article__heading c-heading c-heading--primary c-heading--large">
                    <?php 
                    if ( is_day() ) { echo get_the_time( get_option( 'date_format' ) ); }
                    elseif ( is_month() ) { echo get_the_time( 'F Y' ); }
                    elseif ( is_year() ) { printf( __( 'Rok %s', 'markething' ), get_the_time( 'Y' ) ); }
                    else { _e( 'Archiv článků', 'markething' ); }
                    ?>
                </h2>
                <div class="l-article__wrapper">
                    <div class="l-article__content">
                    </div>
                </div>
            </div>
        </section>

        <div class="l-intro__more" style="padding-top: 40px"> <div class="c-row c-row--large">

        <?php rewind_posts(); ?>

        <?php while ( have_posts() ) : the_post(); ?>
            <?php get_template_part( 'entry' ); ?>
        <?php endwhile; ?>

        </div></div>

    </div>
</section>

<?php get_sidebar(); ?>
<?php get_footer(); ?>
