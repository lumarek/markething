<?php

$category = get_the_category();

if (is_single()): get_template_part( 'entry', 'single' ); 
else : ?>

<div class="c-col c-col--large c-col--2">
    <div id="post-<?php the_ID(); ?>" <?php post_class(array('c-article', 'js-article')); ?>>
        <div class="c-article__img"><a href="<?php the_permalink(); ?>"><?php { the_post_thumbnail(array(500, 235)); } ?></a>
            <div class="c-stamp c-stamp--bottom-left">
            <p class="c-stamp__date"><?php the_time( get_option( 'date_format' ) ); ?></p>
            <div class="c-stamp__link"><a href="<?php comments_link(); ?>"><a href="<?php echo get_category_link( $category[0]->term_id ) ?>"><?php echo $category[0]->cat_name ?></a></a></div>
            </div>
        </div>
        <div class="c-article__info">
            <p class="c-article__heading"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></p>
            <?php get_template_part( 'entry', 'summary' ); ?>
        </div>
    </div>
</div>

<?php endif; ?>