            <footer class="l-footer">
                <div class="l-footer__up c-up js-scroll" data-target="body">
                    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="8.5" viewBox="0 0 19.59 8.51">
                        <polyline points="0.31 8.12 9.79 0.64 19.28 8.12" style="fill: none;stroke: currentColor;stroke-miterlimit: 10" />
                    </svg>
                    <p class="c-up__text">nahoru</p>
                </div>
                <div class="c-container c-container--small">
                    <div class="l-footer__wrapper">
                        <ul class="l-footer__content">
                            <li class="l-footer__content-item">
                                <h3 class="l-footer__heading">Menu</h3>
                                <?php wp_nav_menu(array('theme_location' => 'main-menu', 'container' => '', 'menu_class' => 'l-footer__list'));?>
                            </li>

                            <li class="l-footer__content-item">
                                <h3 class="l-footer__heading">Sledujte nás</h3>
                                <ul class="l-footer__list">
                                    <?php if (get_theme_mod('facebook')) { ?>
                                    <li><a href="<?php echo get_theme_mod('facebook'); ?>" target="_blank">Facebook</a>
                                    </li>
                                    <?php } ?>
                                    <?php if (get_theme_mod('twitter')) { ?>
                                    <li><a href="<?php echo get_theme_mod('twitter'); ?>" target="_blank">Twitter</a>
                                    </li>
                                    <?php } ?>
                                    <?php if (get_theme_mod('instagram')) { ?>
                                    <li><a href="<?php echo get_theme_mod('instagram'); ?>" target="_blank">Instagram</a>
                                    </li>
                                    <?php } ?>
                                    <?php if (get_theme_mod('youtube')) { ?>
                                    <li><a href="<?php echo get_theme_mod('youtube'); ?>" target="_blank">Youtube</a>
                                    </li>
                                    <?php } ?>
                                    <?php if (get_theme_mod('rss')) { ?>
                                    <li><a href="<?php echo get_theme_mod('rss'); ?>" target="_blank">RSS</a>
                                    </li>
                                    <?php } ?>
                                </ul>
                            </li>
                            <li class="l-footer__content-item">
                                <h3 class="l-footer__heading"><?php echo esc_html(get_bloginfo('name')); ?></h3>
                                <p class="l-footer__info"><?php echo get_theme_mod('about_us'); ?></p>
                                <p class="l-footer__info-more">
                                    <?php if(get_theme_mod('about_us-page')) { ?><a href="<?php echo esc_url( get_permalink(get_theme_mod('about_us-page'))); ?>">
                                        Více o Markethingu<svg xmlns="http://www.w3.org/2000/svg" width="14" height="11"
                                            viewBox="0 0 11.71 8.77">
                                            <line y1="4.38" x2="11" y2="4.38" style="fill: none;stroke: currentColor;stroke-miterlimit: 10" />
                                            <polyline points="6.97 0.35 11 4.38 6.97 8.41" style="fill: none;stroke: currentColor;stroke-miterlimit: 10" />
                                        </svg></a>
                                    <?php } ?>
                                </p>
                            </li>
                        </ul>
                        <ul class="l-footer__outro">
                            <li class="l-footer__outro-item">
                                <a href="https://fsv.cuni.cz/" target="_blank">
                                    <img src="<?php echo bloginfo('template_directory'); ?>/img/png/fsvuk.png" srcset="<?php echo bloginfo('template_directory'); ?>/img/png/fsvuk@2x.png 2x" alt="fsv.cuni.cz">
                                </a>
                            </li>
                            <li class="l-footer__outro-item">
                                <div class="l-footer__author">
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                        width="41" height="38" viewBox="0 0 54.19 49.53">
                                        <defs>
                                            <clipPath id="logo-rb">
                                                <rect width="54.19" height="49.53" style="fill: none" />
                                            </clipPath>
                                        </defs>
                                        <g style="clip-path: url(#logo-rb)">
                                            <path d="M23.89,8.83a9.11,9.11,0,0,1-2,6.18,9.85,9.85,0,0,1-6,3l8.82,12.8h-5l-8-12.6H6.29v12.6H2.41V0H13.15q5.2,0,8,2.5a8.15,8.15,0,0,1,2.77,6.33m-11,6.72a7.4,7.4,0,0,0,5.27-1.69,5.85,5.85,0,0,0,1.79-4.46A5.78,5.78,0,0,0,18.18,5a7.05,7.05,0,0,0-5-1.64H6.29V15.54Z"
                                                style="fill: #fff" />
                                            <path d="M30.49,0H42q4.85,0,7.48,2.31a7.38,7.38,0,0,1,2.62,5.78,7.55,7.55,0,0,1-1.45,4.61,7.26,7.26,0,0,1-4.39,2.65,7.58,7.58,0,0,1,4.63,2.5,6.78,6.78,0,0,1,1.74,4.56q0,5.69-5.54,7.7a15.1,15.1,0,0,1-5,.74H30.49Zm3.87,13.78h7.55a6.9,6.9,0,0,0,4.54-1.4,4.72,4.72,0,0,0,1.69-3.85A4.71,4.71,0,0,0,46.5,4.71a7,7,0,0,0-4.58-1.37H34.36ZM42,27.46q3.53,0,5.05-1.42a4.93,4.93,0,0,0,1.52-3.77,4.64,4.64,0,0,0-1.67-3.75,7.46,7.46,0,0,0-4.9-1.4h-7.6V27.46Z"
                                                style="fill: #fff" />
                                            <rect y="46.53" width="54.19" height="3" style="fill: #fff" />
                                        </g>
                                    </svg>
                                    <div class="l-footer__author-wrapper">
                                        <p>© FSV UK 2018</p>
                                        <p>Design by bartos.design</p>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </footer>
        
        
        <!--- CLOSE PAGE CONTENT --->
        </div>

    <!--- CLOSE PAGE --->
    </div>
    <script src="/js/index.js"></script>
    <?php wp_footer(); ?>
</body>

</html>
