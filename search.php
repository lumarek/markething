<?php get_header(); ?>

<?php get_header(); ?>

<section class="l-article">
    <div class="c-container c-container--small">
        
        <section class="l-article">
            <div class="c-container c-container--small">
                <h2 class="l-article__heading c-heading c-heading--primary c-heading--large">Výsledky vyhledávání</h2>
                <?php if (!have_posts()): ?>
                    <p style="margin-top: 20px">Hledaný výraz nebyl nalezen. Zkuste prosím hledání opakovat</p>
                <?php else: ?>
                    <p style="margin-top: 20px">Hledaný výraz: <?php the_search_query(); ?></p>
                <?php endif; ?>
            </div>
        </section>

        <div class="l-intro__more" style="padding-top: 40px"><div class="c-row c-row--large">

        <?php rewind_posts(); ?>      

        <?php while ( have_posts() ) : the_post(); ?>
            <?php get_template_part( 'entry' ); ?>
        <?php endwhile; ?>

        </div></div>

    </div>
</section>

<?php get_sidebar(); ?>
<?php get_footer(); ?>
